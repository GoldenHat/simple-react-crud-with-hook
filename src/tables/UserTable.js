import React from "react";
const UserTable = props => {
  return (
    <>
      <tbody>
        {props.users.length > 0 ? (
          props.users.map(user => (
            <tr key={user.id}>
              <td>{user.BankName}</td>
              <td>{user.AccountOwnerName}</td>
              <td>{user.CardNumber}</td>
              <td>{user.AccountNumber}</td>
              <td>{user.NightNumber}</td>
              <td>
                <button
                  onClick={() => {
                    props.editRow(user);
                  }}
                  className="button muted-button">
                  Edit
                </button>
                <button onClick={() => props.deleteUser(user.id)} className="button muted-button">
                  Delete
                </button>
              </td>
            </tr>
          ))
        ) : (
          <tr>
            <td colSpan={6}>No users</td>
          </tr>
        )}
      </tbody>
    </>
  );
};

export default UserTable;
