import React, { useState, useEffect } from "react";
import UserRows from "../tables/UserTable";

const EditUserForm = props => {
  const [user, setUser] = useState(props.currentUser);

  useEffect(() => {
    setUser(props.currentUser);
  }, [props]);
  // You can tell React to skip applying an effect if certain values haven’t changed between re-renders. [ props ]

  const handleInputChange = event => {
    const { name, value } = event.target;

    setUser({ ...user, [name]: value });
  };

  return (
    <form
      onSubmit={event => {
        event.preventDefault();
        props.updateUser(user.id, user);
      }}>
      <table className="ui table">
        <thead>
          <tr>
            <th>BankName</th>
            <th>AccountOwnerName</th>
            <th>CardNumber</th>
            <th>AccountNumber</th>
            <th>NightNumber</th>
            <th>Action</th>
          </tr>
          <tr>
            <td>
              <input type="text" name="BankName" value={user.BankName} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="AccountOwnerName" value={user.AccountOwnerName} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="CardNumber" value={user.CardNumber} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="AccountNumber" value={user.AccountNumber} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="NightNumber" value={user.NightNumber} onChange={handleInputChange} />
            </td>
            <td>
              <button>Update user</button>
              <button onClick={() => props.setEditing(false)} className="button muted-button">
                Cancel
              </button>
            </td>
          </tr>
        </thead>
        <UserRows users={props.users} editRow={props.editRow} deleteUser={props.deleteUser} />
      </table>
    </form>
  );
};

export default EditUserForm;
