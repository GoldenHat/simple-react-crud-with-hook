import React, { useState } from "react";
import UserRows from "../tables/UserTable";

const AddUserForm = props => {
  const initialFormState = {
    id: null,
    BankName: "",
    AccountOwnerName: "",
    CardNumber: "",
    AccountNumber: "",
    NightNumber: "",
  };

  const [user, setUser] = useState(initialFormState);
  const handleInputChange = event => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  return (
    <form
      onSubmit={event => {
        event.preventDefault();
        const commonState =
          !user.BankName || !user.AccountOwnerName || !user.CardNumber || !user.AccountNumber || !user.NightNumber;
        if (commonState) return;

        props.addUser(user);
        setUser(initialFormState);
      }}>
      <table className="ui table">
        <thead>
          <tr>
            <th>BankName</th>
            <th>AccountOwnerName</th>
            <th>CardNumber</th>
            <th>AccountNumber</th>
            <th>NightNumber</th>
            <th>Action</th>
          </tr>
          <tr>
            <td>
              <input type="text" name="BankName" value={user.BankName} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="AccountOwnerName" value={user.AccountOwnerName} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="CardNumber" value={user.CardNumber} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="AccountNumber" value={user.AccountNumber} onChange={handleInputChange} />
            </td>
            <td>
              <input type="text" name="NightNumber" value={user.NightNumber} onChange={handleInputChange} />
            </td>
            <td>
              <button>Add new user</button>
            </td>
          </tr>
        </thead>
        <UserRows users={props.users} editRow={props.editRow} deleteUser={props.deleteUser} />
      </table>
    </form>
  );
};

export default AddUserForm;
