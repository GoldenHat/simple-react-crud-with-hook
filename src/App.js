import React, { useState, Fragment } from "react";
import AddUserForm from "./forms/AddUserForm";
import EditUserForm from "./forms/EditUserForm";
// import UserTable from "./tables/UserTable";

const App = () => {
  // Data
  const usersData = [
    // {
    //   id: 1,
    //   BankName: "Ben",
    //   AccountOwnerName: "benisphere",
    //   CardNumber: "benisphere",
    //   AccountNumber: "benisphere",
    //   NightNumber: "benisphere",
    // },
  ];

  const initialFormState = {
    id: null,
    BankName: "",
    AccountOwnerName: "",
    CardNumber: "",
    AccountNumber: "",
    NightNumber: "",
  };

  // Setting state
  const [users, setUsers] = useState(usersData);
  const [currentUser, setCurrentUser] = useState(initialFormState);
  const [editing, setEditing] = useState(false);

  // CRUD operations
  const addUser = user => {
    user.id = users.length + 1;
    setUsers([...users, user]);
  };

  const deleteUser = id => {
    setEditing(false);
    setUsers(users.filter(user => user.id !== id));
  };

  const updateUser = (id, updatedUser) => {
    setEditing(false);
    setUsers(users.map(user => (user.id === id ? updatedUser : user)));
  };

  const editRow = user => {
    setEditing(true);

    setCurrentUser({
      id: user.id,
      BankName: user.BankName,
      AccountOwnerName: user.AccountOwnerName,
      CardNumber: user.CardNumber,
      AccountNumber: user.AccountNumber,
      NightNumber: user.NightNumber,
    });
  };

  return (
    <div className="container">
      {editing ? (
        <Fragment>
          <EditUserForm
            users={users}
            editRow={editRow}
            deleteUser={deleteUser}
            editing={editing}
            setEditing={setEditing}
            currentUser={currentUser}
            updateUser={updateUser}
          />
        </Fragment>
      ) : (
        <Fragment>
          <AddUserForm users={users} editRow={editRow} deleteUser={deleteUser} addUser={addUser} />
        </Fragment>
      )}
      {/* <UserTable users={users} editRow={editRow} deleteUser={deleteUser} /> */}
    </div>
  );
};

export default App;
